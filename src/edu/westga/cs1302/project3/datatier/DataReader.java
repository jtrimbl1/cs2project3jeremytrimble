package edu.westga.cs1302.project3.datatier;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import edu.westga.cs1302.project3.model.User;
import edu.westga.cs1302.project3.resources.UI;

/**
 * The DataReader Class
 * 
 * @author jeremy.trimble
 * @version 11/15/2018
 */
public class DataReader {

	/**
	 * Reads in the file and creates a list of strings based on if the line starts
	 * with a "-"
	 * 
	 * @precondition file !=null
	 * @postcondition none
	 * 
	 * @param file
	 *            the file that has the responses list
	 * 
	 * @return a list of the negative message strings
	 */
	public static List<String> getNegativeFeedback(File file) {
		if (file == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_FILE);
		}
		List<String> negativeFeedback = new ArrayList<String>();
		try (Scanner scanner = new Scanner(file)) {
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				if (line.charAt(0) == '-') {
					negativeFeedback.add(line.substring(1));
				}
			}
		} catch (Exception e) {
			System.err.println(e.getMessage());
			negativeFeedback.add(UI.ExceptionMessages.BACKUP_NEGATIVE_REPSONSE);
		}
		return negativeFeedback;
	}

	/**
	 * Reads in the file and creates a list of strings based on if the line starts
	 * with a "+"
	 * 
	 * @precondition file != null
	 * @postcondition none
	 * 
	 * @param file
	 *            the file that has the responses list
	 * 
	 * @return a list of the positive message strings
	 */
	public static List<String> getPostiveFeedback(File file) {
		if (file == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_FILE);
		}
		List<String> positiveFeedback = new ArrayList<String>();
		try (Scanner scanner = new Scanner(file)) {
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				if (line.charAt(0) == '+') {
					positiveFeedback.add(line.substring(1));
				}
			}
		} catch (Exception e) {
			System.err.println(e.getMessage());
			positiveFeedback.add(UI.ExceptionMessages.BACKUP_POSITIVE_RESPONSE);

		}
		return positiveFeedback;
	}

	/**
	 * Reads in a file with the list of existing usernames and score
	 * 
	 * @precondition file != null
	 * @postcondition none
	 * 
	 * @param file
	 *            the file with previous usernames and scores
	 * 
	 * @return a list of all the usernames and scores
	 */
	public static List<User> loadUsers(File file) {
		if (file == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_FILE);
		}
		List<User> users = new ArrayList<User>();
		try (Scanner scanner = new Scanner(file)) {
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] fields = line.split(",");
				User newUser = new User(fields[0]);
				newUser.setScore(Integer.parseInt(fields[1]));
				if (fields.length == 3) {
					newUser.setLongestStreak(Integer.parseInt(fields[2]));
				} else {
					newUser.setLongestStreak(0);
				}
				users.add(newUser);
			}
		} catch (FileNotFoundException fnfe) {
			System.err.println(fnfe.getMessage());
		}
		return users;
	}

}
