package edu.westga.cs1302.project3.datatier;

import java.io.File;
import java.io.FileWriter;
import java.util.List;

import edu.westga.cs1302.project3.model.User;

/**
 * The DataWriter Class
 * 
 * @author jeremy.trimble
 * @version 11/19/2018
 */
public class DataWriter {

	/**
	 * Writes the specified user list to the specified file
	 * 
	 * @param file
	 *            the file to save to
	 * @param users
	 *            the user list to write
	 */
	public static void saveUsersToFile(File file, List<User> users) {
		try (FileWriter writer = new FileWriter(file);) {
			for (User current : users) {
				writer.write(current.getName() + ",");
				writer.write(current.getScore() + ",");
				writer.write(current.getLongestStreak() + System.lineSeparator());

			}
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}

	}
}
