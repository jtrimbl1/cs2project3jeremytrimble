package edu.westga.cs1302.project3.view;

import edu.westga.cs1302.project3.model.User;
import edu.westga.cs1302.project3.viewmodel.PracticeSessionViewModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.util.converter.NumberStringConverter;

/**
 * Code Behind for the Project 3 GUI
 * 
 * @author jeremy.trimble
 *
 */
public class Project3GUICodeBehind {

	@FXML
	private TextField nameTextField;

	@FXML
	private Label answerResponseLabel;

	@FXML
	private Label nameScoreLabel;

	@FXML
	private Label invalidNameLabel;

	@FXML
	private Label scoreLabel;

	@FXML
	private Label mathProblemTextField;

	@FXML
	private Button startPracticeButton;

	@FXML
	private Button submitButton;

	@FXML
	private Button nextButton;

	@FXML
	private TextField answerTextField;

	@FXML
	private Label problemCounterLabel;

	@FXML
	private Label scorePercentLabel;

	@FXML
	private ListView<User> usersListView;

	@FXML
	private Button clearAllButton;

	@FXML
	private Button removeButton;

	@FXML
	private Label incorrectCountLabel;

	@FXML
	private Label streakLabel;

	@FXML
	private Button saveButton;

	@FXML
	private ComboBox<String> equationCombo;

	private PracticeSessionViewModel viewmodel;

	/**
	 * Creates a code behind object
	 */
	public Project3GUICodeBehind() {
		this.viewmodel = new PracticeSessionViewModel();
	}

	/**
	 * Binds the properties of the viewmodel object to the view
	 * 
	 */
	public void initialize() {
		this.invalidNameLabel.setVisible(false);
		this.startPracticeButton.disableProperty().set(true);
		this.submitButton.disableProperty().set(true);
		this.nextButton.disableProperty().set(true);
		this.answerTextField.textProperty().bindBidirectional(this.viewmodel.answerProperty(),
				new NumberStringConverter());
		this.nameTextField.textProperty().bindBidirectional(this.viewmodel.usernameProperty());
		this.mathProblemTextField.textProperty().bindBidirectional(this.viewmodel.mathProblemProperty());
		this.scoreLabel.textProperty().bindBidirectional(this.viewmodel.scoreProperty(), new NumberStringConverter());
		this.answerResponseLabel.textProperty().bindBidirectional(this.viewmodel.answerResponseProperty());
		this.scorePercentLabel.textProperty().bind(this.viewmodel.scorePercentProperty());
		this.equationCombo.itemsProperty().bind(this.viewmodel.operatorsProperty());

		this.setupScoreLabelListener();
		this.setupSessionOverListener();
		this.setupNameFieldListener();
		// this.setupAnswerFieldListener();
		this.setupListSelectionListener();
	}

	private void setupListSelectionListener() {
		this.usersListView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue != null) {
				this.nameTextField.textProperty().set(newValue.getName());
			}
		});

	}

	// private void setupAnswerFieldListener() {
	// this.answerTextField.textProperty().addListener((observable, oldValue,
	// newValue) -> {
	// if (!newValue.matches("//d") && !oldValue.isEmpty()) {
	// this.answerTextField.textProperty().set(oldValue);
	// this.submitButton.disableProperty().set(true);
	// } else if (!newValue.matches("//d") && oldValue.isEmpty()) {
	// this.answerTextField.textProperty().set("");
	// this.submitButton.disableProperty().set(true);
	// } else {
	// this.submitButton.disableProperty().set(false);
	// }
	// });
	// }

	private void setupNameFieldListener() {
		this.nameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			if (!newValue.isEmpty()) {
				this.startPracticeButton.disableProperty().set(false);
			} else {
				this.startPracticeButton.disableProperty().set(true);
			}
		});
	}

	@FXML
	void nextQuestion(ActionEvent event) {
		this.answerTextField.textProperty().set("");
		this.submitButton.disableProperty().set(false);
		this.viewmodel.newPracticeProblem();
		this.answerTextField.requestFocus();
		this.answerResponseLabel.textProperty().set("");
		this.problemCounterLabel.textProperty().set(this.viewmodel.getProblemCounter());
	}

	@FXML
	void startPractice(ActionEvent event) {
		if (this.nameTextField.getText() != null && !this.nameTextField.getText().isEmpty()) {
			this.invalidNameLabel.setVisible(false);
			this.viewmodel.startSession(this.nameTextField.getText());
			this.nameScoreLabel.textProperty().set(this.viewmodel.usernameProperty().get() + "'s score:");
			this.answerTextField.requestFocus();
			this.submitButton.disableProperty().set(false);
			this.nextButton.disableProperty().set(false);
			this.problemCounterLabel.textProperty().set(this.viewmodel.getProblemCounter());
			this.usersListView.itemsProperty().bind(this.viewmodel.userListProperty());
			this.incorrectCountLabel.textProperty().bind(this.viewmodel.incorrectAnswerProperty());
			this.streakLabel.textProperty().bind(this.viewmodel.streakProperty());
		} else {
			this.invalidNameLabel.setVisible(true);
		}

	}

	@FXML
	void submitAnswer(ActionEvent event) {
		this.viewmodel.checkAnswer();

	}

	private void setupSessionOverListener() {
		this.viewmodel.sessionOverProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue) {
				this.nextButton.disableProperty().set(true);
				this.submitButton.disableProperty().set(true);
			}
		});
	}

	private void setupScoreLabelListener() {
		this.scoreLabel.textProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue != oldValue) {
				this.submitButton.disableProperty().set(true);
			}
		});
	}

	@FXML
	void clearAll(ActionEvent event) {
		this.viewmodel.clearUsers();
	}

	@FXML
	void remove(ActionEvent event) {
		this.viewmodel.removeUser(this.usersListView.getSelectionModel().getSelectedItem());
	}

	@FXML
	void saveUsers(ActionEvent event) {
		this.viewmodel.saveUsers();
	}
}
