package edu.westga.cs1302.project3.model;

import edu.westga.cs1302.project3.resources.UI;

/**
 * The User Class
 * 
 * @author jeremy.trimble
 * @version 11/12/2018
 */
public class User implements Comparable<User> {

	private String name;
	private int score;
	private int longestStreak;

	/**
	 * Creates a new user with specified name
	 * 
	 * @precondition name !=null && !name.isEmpty()
	 * @postcondition none
	 * @param name
	 *            the user's name
	 */
	public User(String name) {
		if (name == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_NAME);
		}
		if (name.isEmpty()) {
			throw new IllegalArgumentException(UI.ExceptionMessages.EMPTY_NAME);
		}

		this.name = name;
		this.score = 0;
	}

	/**
	 * Returns the user's name
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the user's name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the user's name
	 * 
	 * @precondition name !=null && !name.isEmpty()
	 * 
	 * @param name
	 *            the user's name
	 */
	public void setName(String name) {
		if (name == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_NAME);
		}
		if (name.isEmpty()) {
			throw new IllegalArgumentException(UI.ExceptionMessages.EMPTY_NAME);
		}
		this.name = name;
	}

	/**
	 * Returns the user's score
	 * 
	 * @return the user's score
	 */
	public int getScore() {
		return this.score;
	}

	/**
	 * Sets the users score
	 * 
	 * @precondition score >= 0 && score <= 10
	 * @param score
	 *            the number to set the score to.
	 */
	public void setScore(int score) {
		if (score < 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.SCORE_BELOW_ZERO);
		}
		if (score > 10) {
			throw new IllegalArgumentException(UI.ExceptionMessages.SCORE_ABOVE_TEN);
		}

		this.score = score;

	}

	/**
	 * Returns the longest streak
	 * 
	 * @return the longest streak
	 */
	public int getLongestStreak() {
		return this.longestStreak;
	}

	/**
	 * Sets the longest Streak
	 * 
	 * @param longestStreak
	 *            the streak to be set
	 */
	public void setLongestStreak(int longestStreak) {
		this.longestStreak = longestStreak;
	}

	@Override
	public String toString() {
		return this.name + " scored: " + this.score + " Longest Streak: " + this.longestStreak;
	}

	@Override
	public int compareTo(User other) {
		if (this.score > other.score) {
			return -1;
		} else if (this.score < other.score) {
			return 1;
		}
		return 0;
	}

}
