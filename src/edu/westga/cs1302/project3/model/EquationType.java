package edu.westga.cs1302.project3.model;

/**
 * The enum EqationType
 * 
 * @author jeremy.trimble
 * @version 11/18/2018
 */
public enum EquationType {
	ADDITION, SUBSTRACTION;
}
