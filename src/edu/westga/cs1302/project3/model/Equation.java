package edu.westga.cs1302.project3.model;

import edu.westga.cs1302.project3.resources.UI;

/**
 * The PracticeProblem Class
 * 
 * @author jeremy.trimble
 * @version 12/13/2018
 */
public class Equation {
	private int firstNumber;
	private int secondNumber;
	private int answer;

	/**
	 * Creates a new practice problem
	 * 
	 * @precondition firstNumber >=0 && firstNumber <= 100 && secondNumber >= 0 &&
	 *               secondNumber <=100
	 * @param firstNumber
	 *            the firstNumber
	 * @param secondNumber
	 *            the secondNumber
	 */
	public Equation(int firstNumber, int secondNumber) {
		if (firstNumber < 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NUMBER_LESS_THAN_ZERO);
		}
		if (firstNumber > 100) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NUMBER_GREATER_THAN_ONEHUNDRED);
		}
		if (secondNumber < 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NUMBER_LESS_THAN_ZERO);
		}
		if (secondNumber > 100) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NUMBER_GREATER_THAN_ONEHUNDRED);
		}

		this.firstNumber = firstNumber;
		this.secondNumber = secondNumber;
		this.answer = firstNumber + secondNumber;

	}

	/**
	 * Returns the first number
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the first number
	 */
	public int getFirstNumber() {
		return this.firstNumber;
	}

	/**
	 * Returns the second number
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the second number
	 */
	public int getSecondNumber() {
		return this.secondNumber;
	}

	/**
	 * Returns the answer
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the answer
	 */
	public int getAnswer() {
		return this.answer;
	}

	@Override
	public String toString() {
		return this.firstNumber + " + " + this.secondNumber + " =";
	}

}
