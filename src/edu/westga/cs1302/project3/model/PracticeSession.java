package edu.westga.cs1302.project3.model;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import edu.westga.cs1302.project3.datatier.DataReader;
import edu.westga.cs1302.project3.datatier.DataWriter;

/**
 * The PracticSession class
 * 
 * @author jeremy.trimble
 * @version 11/13/2018
 */
public class PracticeSession {
	private User user;
	private Equation equation;
	private Random random;
	private Integer score;
	private int currentQuestion;
	private List<String> negativeFeedbackResponses;
	private List<String> positiveFeedbackResponses;
	private List<User> userList;
	private String response;
	private int incorrectAnswers;
	private int totalAnswers;
	private int attempts;
	private int longestStreak;
	private int currentStreak;

	public static final int MAX_QUESTIONS = 10;
	private static final String FEEDBACKRESPONSEFILE = "feedbackresponses.csv";
	private static final String USERSFILE = "usersFile.csv";

	/**
	 * Creates a new Practice Session with the defined user
	 * 
	 * @precondition user != null
	 * @postcondition none
	 * 
	 * @param name
	 *            the name of the user
	 * 
	 */
	public PracticeSession(String name) {
		this.user = new User(name);
		this.random = new Random();
		this.score = 0;
		this.currentQuestion = 0;
		this.totalAnswers = 0;
		this.incorrectAnswers = 0;
		this.attempts = 0;
		this.longestStreak = 0;
		this.currentStreak = 0;
		this.response = "";
		File responses = new File(PracticeSession.FEEDBACKRESPONSEFILE);
		File usersFile = new File(PracticeSession.USERSFILE);
		this.negativeFeedbackResponses = DataReader.getNegativeFeedback(responses);
		this.positiveFeedbackResponses = DataReader.getPostiveFeedback(responses);
		this.userList = DataReader.loadUsers(usersFile);
		Collections.sort(this.userList);

	}

	/**
	 * Generates an equation
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 */
	public void generateEquation() {
		if (!this.sessionOver()) {
			int firstNumber = this.random.nextInt(100);
			int secondNumber = this.random.nextInt(100);
			this.equation = new Equation(firstNumber, secondNumber);
			this.currentQuestion++;
		}

	}

	/**
	 * Checks if the passed in value is correct for the current equation
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param answer
	 *            the answer to check
	 * @return true if the provided answer is correct, else false
	 */
	public boolean checkAnswer(int answer) {
		if (this.sessionOver()) {
			return false;
		}
		if (this.equation.getAnswer() == answer) {
			if (this.attempts == 0) {
				this.currentStreak++;
			}
			this.response = this.setResponse(true);
			this.addToScore();
			this.attempts = 0;
			this.totalAnswers++;
			return true;
		} else {
			this.currentStreak = 0;
			this.totalAnswers++;
			this.incorrectAnswers++;
			this.attempts++;
			this.response = this.setResponse(false);
			return false;
		}
	}

	private void addToScore() {
		if (this.attempts == 0) {
			this.score += 3;
		} else if (this.attempts == 1) {
			this.score += 2;
		} else if (this.attempts == 2) {
			this.score += 1;
		}
	}

	/**
	 * Returns the current equation as a string
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the currentEquation
	 */
	public String getEquation() {
		return this.equation.toString();
	}

	/**
	 * Returns the username
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the username
	 */
	public String getUsername() {
		return this.user.getName();
	}

	/**
	 * Returns the current question number
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the current question number
	 */
	public int getCurrentQuestion() {
		return this.currentQuestion;
	}

	/**
	 * Returns the current score
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the current score
	 */
	public Integer getScore() {
		return this.score;
	}

	/**
	 * Returns if the practice session is over
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return true if the session has asked the max number of questions
	 */
	public boolean sessionOver() {
		if (this.currentQuestion < PracticeSession.MAX_QUESTIONS) {
			return false;
		} else {
			this.addUserToList();
			return true;
		}

	}

	private void addUserToList() {
		boolean exist = false;
		for (User curr : this.userList) {
			if (this.user.getName().equalsIgnoreCase(curr.getName())) {
				curr.setScore(this.user.getScore());
				exist = true;
			}
		}
		if (!exist) {
			this.userList.add(this.user);
		}

		Collections.sort(this.userList);

	}

	private String setResponse(Boolean bool) {
		if (bool) {
			return this.positiveFeedbackResponses.get(this.random.nextInt(this.positiveFeedbackResponses.size()));
		} else {
			return this.negativeFeedbackResponses.get(this.random.nextInt(this.negativeFeedbackResponses.size()));
		}
	}

	/**
	 * Returns a response
	 * 
	 * @return a response
	 */
	public String getResponse() {
		return this.response;
	}

	/**
	 * Returns the list of users
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the list of users
	 */
	public List<User> getUsersList() {
		return this.userList;
	}

	/**
	 * Clears the user list
	 * 
	 * @precondition none
	 * @postcondition the userlist is empty
	 * 
	 */
	public void clearUsers() {
		this.userList.clear();
	}

	/**
	 * Removes the user from the userlist
	 * 
	 * @precondition userlist contain user
	 * @postcondition userlist.size() == userlist.size()@prev - 1
	 * @param user
	 *            the user to be removed
	 * 
	 */
	public void removeUser(User user) {
		this.userList.remove(user);
	}

	/**
	 * Calculates the percentage of correct answers
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return a string that give the percentage of correct answers
	 */
	public String calculatePercentage() {
		String result = "Percentage of Correct Answers: ";
		double percent = ((this.totalAnswers - this.incorrectAnswers) / (double) this.currentQuestion) * 100;
		double rounded = Math.round(percent * 100) / 100.0;
		result += rounded + "%";
		return result;
	}

	/**
	 * Returns the total of incorrect answers
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the number of incorrect answers
	 */
	public int getIncorrectAnswers() {
		return this.incorrectAnswers;
	}

	/**
	 * Returns the total answers given
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the total answers given
	 */
	public int getTotalAnswers() {
		return this.totalAnswers;
	}

	/**
	 * Displays the number of incorrect answers out of total asked questions
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return a string that tells how many incorrect answers there have been.
	 */
	public String displayIncorrectAnswers() {
		String result = this.incorrectAnswers + " out of " + this.totalAnswers + " answers were incorrect";
		return result;
	}

	/**
	 * Displays the number of incorrect answers out of total asked questions
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return a string that tells how many incorrect answers there have been.
	 */
	public String displayLongestStreak() {
		if (this.currentStreak > this.longestStreak) {
			this.longestStreak = this.currentStreak;
		}
		String result = "Longest Streak: " + this.longestStreak;
		return result;
	}

	/**
	 * Saves users to the file
	 * 
	 * @precondition none
	 * @postcondition the file has all the users in the list written to it
	 *
	 */
	public void saveUsers() {
		DataWriter.saveUsersToFile(new File(PracticeSession.USERSFILE), this.userList);
	}
}
