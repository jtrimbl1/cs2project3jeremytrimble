package edu.westga.cs1302.project3.resources;

/**
 * The UI Class
 * 
 * @author jeremy.trimble
 * @version 11/12/2018
 */
public final class UI {

	/**
	 * Defines Strings for thrown exception messages
	 */
	public static final class ExceptionMessages {
		public static final String NULL_NAME = "Name cannot be Null";
		public static final String EMPTY_NAME = "Name cannot be empty";
		public static final String SCORE_BELOW_ZERO = "Score must be greater than or equal to 0.";
		public static final String SCORE_ABOVE_TEN = "Score must be less than or equal to 10.";
		public static final String NUMBER_LESS_THAN_ZERO = "Number must be greater than or equal to zero.";
		public static final String NUMBER_GREATER_THAN_ONEHUNDRED = "Number must be less than or equal to 100.";
		public static final String NULL_USER = "User cannot be null.";
		public static final String EMPTY_EQUATIONS_LIST = "Equations must be generated first";
		public static final String OUT_OF_QUESTIONS = "No more questions to answer";
		public static final String NULL_FILE = "File cannot be null.";
		public static final String BACKUP_POSITIVE_RESPONSE = "You Got It!";
		public static final String BACKUP_NEGATIVE_REPSONSE = "Wrong! Try again.";
	}
}
