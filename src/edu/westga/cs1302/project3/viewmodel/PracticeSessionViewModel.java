package edu.westga.cs1302.project3.viewmodel;

import edu.westga.cs1302.project3.model.PracticeSession;
import edu.westga.cs1302.project3.model.User;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * The PracticeSessionViewModel Class
 * 
 * @author jeremy.trimble
 * @version 11/13/2018
 */
public class PracticeSessionViewModel {

	private PracticeSession session;
	private StringProperty usernameProperty;
	private IntegerProperty answerProperty;
	private IntegerProperty scoreProperty;
	private StringProperty mathProblemProperty;
	private StringProperty answerResponseProperty;
	private BooleanProperty sessionOverProperty;
	private ListProperty<User> userListProperty;
	private StringProperty scorePercentProperty;
	private StringProperty incorrectAnswerProperty;
	private ListProperty<String> operatorsProperty;
	private StringProperty streakProperty;

	/**
	 * Creates a Practice Session Viewmodel
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 */
	public PracticeSessionViewModel() {
		this.usernameProperty = new SimpleStringProperty();
		this.answerProperty = new SimpleIntegerProperty();
		this.scoreProperty = new SimpleIntegerProperty();
		this.mathProblemProperty = new SimpleStringProperty();
		this.answerResponseProperty = new SimpleStringProperty();
		this.sessionOverProperty = new SimpleBooleanProperty(false);
		this.userListProperty = new SimpleListProperty<User>();
		this.scorePercentProperty = new SimpleStringProperty();
		this.incorrectAnswerProperty = new SimpleStringProperty();
		this.operatorsProperty = new SimpleListProperty<String>();
		this.operatorsProperty.set(FXCollections.observableArrayList("Addition", "Subtraction", "Random"));
		this.streakProperty = new SimpleStringProperty();
	}

	/**
	 * Returns the username property
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the username property
	 */
	public StringProperty usernameProperty() {
		return this.usernameProperty;
	}

	/**
	 * Returns the Operator property
	 * 
	 * @return the operator property
	 */
	public ListProperty<String> operatorsProperty() {
		return this.operatorsProperty;
	}

	/**
	 * 
	 * Returns the answer property
	 *
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the answer property
	 */
	public IntegerProperty answerProperty() {
		return this.answerProperty;
	}

	/**
	 * Returns the score property
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the score property
	 */
	public IntegerProperty scoreProperty() {
		return this.scoreProperty;
	}

	/**
	 * Returns the math problem property
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the math problem property
	 */
	public StringProperty mathProblemProperty() {
		return this.mathProblemProperty;
	}

	/**
	 * Returns the streak property
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the streak property
	 */
	public StringProperty streakProperty() {
		return this.streakProperty;
	}

	/**
	 * Returns the answer response property
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the answer response property
	 */
	public StringProperty answerResponseProperty() {
		return this.answerResponseProperty;
	}

	/**
	 * 
	 * Returns the user list property
	 *
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the user list property
	 */
	public ListProperty<User> userListProperty() {
		return this.userListProperty;
	}

	/**
	 * Returns the score percent property
	 * 
	 * @return the score percent property
	 */
	public StringProperty scorePercentProperty() {
		return this.scorePercentProperty;
	}

	/**
	 * Starts a new session
	 * 
	 * @precondition name != null && !name.isEmpty()
	 * @postcondition new session is created
	 * 
	 * @param name
	 *            the name of the user
	 *
	 */
	public void startSession(String name) {
		this.session = new PracticeSession(name);
		this.usernameProperty.set(this.session.getUsername());
		this.newPracticeProblem();
		this.scoreProperty.set(this.session.getScore());
		this.incorrectAnswerProperty.set(this.session.displayIncorrectAnswers());
		ObservableList<User> users = FXCollections.observableArrayList(this.session.getUsersList());
		this.userListProperty.set(users);
		this.streakProperty.set(this.session.displayLongestStreak());
	}

	/**
	 * Generates a new practice problem
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 */
	public void newPracticeProblem() {
		this.scorePercentProperty.set(this.session.calculatePercentage());
		if (this.session.sessionOver()) {
			this.userListProperty.set(FXCollections.observableArrayList(this.session.getUsersList()));
			this.sessionOverProperty.set(true);
		} else {
			this.session.generateEquation();
			this.mathProblemProperty.set(this.session.getEquation());
		}

	}

	/**
	 * Checks the practice problem and updates the score.
	 * 
	 * @precondition this.answerProperty.get() != null
	 * @postcondition none
	 *
	 */
	public void checkAnswer() {
		try {
			if (this.session.checkAnswer(this.answerProperty.get())) {
				this.scoreProperty.set(this.session.getScore());
				this.answerResponseProperty.set(this.session.getResponse());
				this.scorePercentProperty.set(this.session.calculatePercentage());
				this.incorrectAnswerProperty.set(this.session.displayIncorrectAnswers());
				this.streakProperty.set(this.session.displayLongestStreak());
			} else {
				this.answerResponseProperty.set(this.session.getResponse());
				this.incorrectAnswerProperty.set(this.session.displayIncorrectAnswers());
				this.streakProperty.set(this.session.displayLongestStreak());
			}
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}

	}

	/**
	 * Returns the session over property
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the session over property
	 */
	public BooleanProperty sessionOverProperty() {
		return this.sessionOverProperty;
	}

	/**
	 * Display the current problem number the user is on out of the total number of
	 * practice problems
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return a string describing the current problem counter
	 */
	public String getProblemCounter() {
		return "Practice problem #" + this.session.getCurrentQuestion() + " of out " + PracticeSession.MAX_QUESTIONS;
	}

	/**
	 * Clears the user list.
	 * 
	 * @precondition none
	 * @postcondition userList is empty
	 */
	public void clearUsers() {
		this.session.clearUsers();
		this.userListProperty.set(FXCollections.observableArrayList(this.session.getUsersList()));
	}

	/**
	 * Removes the specified user
	 * 
	 * @precondition user is apart of the list
	 * @postcondition userlist.size() = userList.size()@prev -1
	 * @param user
	 *            the user that is being removed
	 * 
	 */
	public void removeUser(User user) {
		this.session.removeUser(user);
		this.userListProperty.set(FXCollections.observableArrayList(this.session.getUsersList()));
	}

	/**
	 * Returns the incorrect Answer Property
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the incorrect answer property
	 */
	public StringProperty incorrectAnswerProperty() {
		return this.incorrectAnswerProperty;
	}

	/**
	 * Saves the users to the userlist file
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 */
	public void saveUsers() {
		this.session.saveUsers();
	}

}
